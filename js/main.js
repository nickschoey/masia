jQuery(document).ready(function($) {
  var headerHeight = $("#header").outerHeight(); //gets height of header
  var topOfHero = $("#hero").offset().top;
  var topOfAbout = $("#about").offset().top;
  var topOfResa = $("#call-to-action").offset().top;
  var topOfContact = $("#contact").offset().top;
  var topOfFormContact = $("#formContact").offset().top;

  // Header fixed and Back to top button
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $(".back-to-top").fadeIn("slow");
      $("#header").addClass("header-fixed");
    } else {
      $(".back-to-top").fadeOut("slow");
      $("#header").removeClass("header-fixed");
    }
    if ($(this).scrollTop() > topOfHero - headerHeight) {
      $(".nav-menu")
        .find(".menu-active")
        .removeClass("menu-active");
      $(".hero").addClass("menu-active");
    }
    if ($(this).scrollTop() > topOfAbout - headerHeight) {
      $(".nav-menu")
        .find(".menu-active")
        .removeClass("menu-active");
      $(".about").addClass("menu-active");
    }
    if ($(this).scrollTop() > topOfResa - headerHeight) {
      $(".nav-menu")
        .find(".menu-active")
        .removeClass("menu-active");
      $(".call-to-action").addClass("menu-active");
    }
    if ($(this).scrollTop() > topOfContact + 400 - headerHeight) {
      $(".nav-menu")
        .find(".menu-active")
        .removeClass("menu-active");
      $(".contact").addClass("menu-active");
    }
    if ($(this).scrollTop() > topOfFormContact) {
      $(".nav-menu")
        .find(".menu-active")
        .removeClass("menu-active");
      $(".formContact").addClass("menu-active");
    }
  });
  $(".back-to-top").click(function() {
    $("html, body").animate({ scrollTop: 0 }, 1500, "easeInOutExpo");
    return false;
  });

  // Initiate the wowjs
  new WOW().init();

  // Initiate superfish on nav menu
  $(".nav-menu").superfish({
    animation: { opacity: "show" },
    speed: 400
  });

  // Mobile Navigation
  if ($("#nav-menu-container").length) {
    var $mobile_nav = $("#nav-menu-container")
      .clone()
      .prop({ id: "mobile-nav" });
    $mobile_nav.find("> ul").attr({ class: "", id: "" });
    $("body").append($mobile_nav);
    $("body").prepend(
      '<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>'
    );
    $("body").append('<div id="mobile-body-overly"></div>');
    $("#mobile-nav")
      .find(".menu-has-children")
      .prepend('<i class="fa fa-chevron-down"></i>');

    $(document).on("click", ".menu-has-children i", function(e) {
      $(this)
        .next()
        .toggleClass("menu-item-active");
      $(this)
        .nextAll("ul")
        .eq(0)
        .slideToggle();
      $(this).toggleClass("fa-chevron-up fa-chevron-down");
    });

    $(document).on("click", "#mobile-nav-toggle", function(e) {
      $("body").toggleClass("mobile-nav-active");
      $("#mobile-nav-toggle i").toggleClass("fa-times fa-bars");
      $("#mobile-body-overly").toggle();
    });

    $(document).click(function(e) {
      var container = $("#mobile-nav, #mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($("body").hasClass("mobile-nav-active")) {
          $("body").removeClass("mobile-nav-active");
          $("#mobile-nav-toggle i").toggleClass("fa-times fa-bars");
          $("#mobile-body-overly").fadeOut();
        }
      }
    });
  } else if ($("#mobile-nav, #mobile-nav-toggle").length) {
    $("#mobile-nav, #mobile-nav-toggle").hide();
  }

  // Smoth scroll on page hash links
  $('a[href*="#"]:not([href="#"])').on("click", function() {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      if (target.length) {
        var top_space = 0;

        if ($("#header").length) {
          top_space = $("#header").outerHeight();

          if (!$("#header").hasClass("header-fixed")) {
            top_space = top_space - 20;
          }
        }

        $("html, body").animate(
          {
            scrollTop: target.offset().top - top_space
          },
          1500,
          "easeInOutExpo"
        );

        if ($(this).parents(".nav-menu").length) {
          $(".nav-menu .menu-active").removeClass("menu-active");
          $(this)
            .closest("li")
            .addClass("menu-active");
        }

        if ($("body").hasClass("mobile-nav-active")) {
          $("body").removeClass("mobile-nav-active");
          $("#mobile-nav-toggle i").toggleClass("fa-times fa-bars");
          $("#mobile-body-overly").fadeOut();
        }
        return false;
      }
    }
  });

  // jQuery counterUp
  $('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000
  });

  //Google Map
  var get_latitude = $("#google-map").data("latitude");
  var get_longitude = $("#google-map").data("longitude");

  function initialize_google_map() {
    var myLatlng = new google.maps.LatLng(get_latitude, get_longitude);
    var mapOptions = {
      zoom: 11,
      scrollwheel: false,
      center: myLatlng
    };
    var map = new google.maps.Map(
      document.getElementById("google-map"),
      mapOptions
    );
    var image = {
      url: "img/logoacolor.png",
      scaledSize: new google.maps.Size(40, 40) // scaled size
    };
    var marker = new google.maps.Marker({
      title: "Masia Can Cardús",
      position: myLatlng,
      map: map
    });
  }
  google.maps.event.addDomListener(window, "load", initialize_google_map);

  //carousel
  $(".carousel").carousel();

  // Calendar
  var startDate = "";
  var endDate = "";
  var blockDates = [
    //Març
    "18-2-2018",
    "29-2-2018",
    "30-2-2018",
    "31-2-2018",
    //Abril
    "1-3-2018",
    "2-3-2018",
    "13-3-2018",
    "14-3-2018",
    "15-3-2018",
    //Maig
    "4-4-2018",
    "5-4-2018",
    "6-4-2018",
    "11-4-2018",
    "12-4-2018",
    "13-4-2018",
    "14-4-2018",
    "15-4-2018",
    "31-4-2018",
    //Juny
    "1-5-2018",
    "2-5-2018",
    "8-5-2018",
    "9-5-2018",
    "10-5-2018",
    "15-5-2018",
    "16-5-2018",
    "17-5-2018",
    //Juliol
    "14-6-2018",
    "15-6-2018",
    "16-6-2018",
    "17-6-2018",
    "18-6-2018",
    "19-6-2018",
    "20-6-2018",
    "21-6-2018",
    "22-6-2018",
    "23-6-2018",
    "24-6-2018",
    "25-6-2018",
    "26-6-2018",
    "27-6-2018",
    "28-6-2018",
    "29-6-2018",
    "30-6-2018",
    "31-6-2018",
    //Agost
    "1-7-2018",
    "2-7-2018",
    "3-7-2018",
    "4-7-2018",
    "5-7-2018",
    "6-7-2018",
    "7-7-2018",
    "8-7-2018",
    "9-7-2018",
    "10-7-2018",
    "11-7-2018",
    "12-7-2018",
    "13-7-2018",
    "14-7-2018",
    "15-7-2018",
    "16-7-2018",
    "17-7-2018",
    "18-7-2018",
    "19-7-2018",
    "20-7-2018",
    "21-7-2018",
    "22-7-2018",
    "23-7-2018",
    "24-7-2018",
    "25-7-2018",
    //Setembre
    "14-8-2018",
    "15-8-2018",
    "16-8-2018",
    "30-8-2018",
    // Octubre
    "1-9-2018",
    "2-9-2018",
    "3-9-2018",
    "4-9-2018",
    "5-9-2018",
    "6-9-2018",
    "7-9-2018"
    //Novembre
    //Octubre
    //Desembre
  ];

  $("#date-range12")
    .dateRangePicker({
      language: "cat",
      customArrowPrevSymbol: '<i class="fa fa-arrow-circle-left"></i>',
      customArrowNextSymbol: '<i class="fa fa-arrow-circle-right"></i>',
      startOfWeek: "monday",
      separator: " ",
      format: "YYYY-MM-DD",
      inline: true,
      container: ".calendar",
      alwaysOpen: true,
      customTopBar: "Esculli les seves dates",
      selectForward: true,
      stickyMonths: true,
      startDate: moment(),
      singleMonth: "auto",
      showTopbar: false,
      hoveringTooltip: function(days) {
        var nits = days -1;
        if (nits == 1) {  
          return "1 nit";     
        } else {
          return nits + " nits";
        }
      },

      beforeShowDay: function(t) {
        var fullDate = t.getDate() + "-" + t.getMonth() + "-" + t.getFullYear();
        var valid = !(blockDates.indexOf(fullDate) != -1);
        var _class = "";
        var _tooltip = valid ? "" : "Data reservada";
        return [valid, _class, _tooltip];
      }
    })
    .bind("datepicker-change", function(event, obj) {
      /* This event will be triggered when second date is selected */
      startDate = obj.value.substr(0, obj.value.indexOf(" "));
      endDate = obj.value.substr(obj.value.indexOf(" ") + 1);
      $("#dataSortir").val(
        moment(obj.date2)
          .locale("ca")
          .format("dd D MMMM YYYY")
      );
      $("#dataSortir")
        .parent()
        .addClass("is-active is-completed");

      // obj will be something like this:
      // {
      // 		date1: (Date object of the earlier date),
      // 		date2: (Date object of the later date),
      //	 	value: "2013-06-05 to 2013-06-07"
      // }
    })
    .bind("datepicker-first-date-selected", function(event, obj) {
      /* This event will be triggered when first date is selected */
      $("#dataArribar").val(
        moment(obj.date1)
          .locale("ca")
          .format("dd D MMMM YYYY")
      );

      $("#dataArribar")
        .parent()
        .addClass("is-active is-completed");
      // obj will be something like this:
      // {
      // 		date1: (Date object of the earlier date)
      // }
    });

  $("#reservation").click(function() {
    var link =
      "https://www.avaibook.com/reservas/nueva_reserva.php?idw=9901&f_ini=" +
      startDate +
      "&f_fin=" +
      endDate +
      "&lang=ca&cod_alojamiento=7824&cod_unidad_alojativa=15229";
    var win = window.open(link, "_blank");
    if (win) {
      //Browser has allowed it to be opened
      win.focus();
    } else {
      //Browser has blocked it
      alert("Permeti les finestres emergents per aquesta pàgina");
    }
  });
  !$(".mat-input").focus(function() {
    $(this)
      .parent()
      .addClass("is-active is-completed");
  });

  $(".mat-input").focusout(function() {
    if ($(this).val() === "")
      $(this)
        .parent()
        .removeClass("is-completed");
    $(this)
      .parent()
      .removeClass("is-active");
  });
});
